// -----------
// Collatz.hpp
// -----------

#ifndef Collatz_hpp
#define Collatz_hpp

// ----------------
// max_cycle_length
// ----------------

/**
 * @param two positive ints
 * @return one positive int
 */


unsigned max_cycle_length (long i, long j);

unsigned calc_cycle_length(long i);

#endif // Collatz_hpp
