// ---------------
// TestCollatz.cpp
// ---------------

// https://code.google.com/p/googletest/wiki/V1_7_Primer#Basic_Assertions

// --------
// includes
// --------

#include <sstream>  // istringtstream, ostringstream
#include <tuple>    // make_tuple, tuple
#include <utility>  // make_pair, pair

#include "gtest/gtest.h"

#include "Collatz.hpp"

using namespace std;

// ----------------
// max_cycle_length
// ----------------

TEST(CollatzFixture, max_cycle_length_0) {
    ASSERT_EQ(max_cycle_length(1, 10), 20u);
}

TEST(CollatzFixture, max_cycle_length_1) {
    ASSERT_EQ(max_cycle_length(100, 200), 125u);
}

TEST(CollatzFixture, max_cycle_length_2) {
    ASSERT_EQ(max_cycle_length(201, 210), 89u);
}

TEST(CollatzFixture, max_cycle_length_3) {
    ASSERT_EQ(max_cycle_length(900, 1000), 174u);
}

TEST(CollatzFixture, max_cycle_length_4) {
    ASSERT_EQ(max_cycle_length(1, 1000000), 525u);
}

TEST(CollatzFixture, max_cycle_length_5) {
    ASSERT_EQ(max_cycle_length(267654, 274372), 407u);
}

TEST(CollatzFixture, max_cycle_length_6) {
    ASSERT_EQ(max_cycle_length(10, 10), 7u);
}

TEST(CollatzFixture, max_cycle_length_7) {
    ASSERT_EQ(max_cycle_length(10, 1), 20u);
}

TEST(CollatzFixture, max_cycle_length_8) {
    ASSERT_EQ(max_cycle_length(16, 17), 13u);
}

TEST(CollatzFixture, max_cycle_length_9) {
    ASSERT_EQ(max_cycle_length(10, 9), 20u);
}

TEST(CollatzFixture, max_cycle_length_10) {
    ASSERT_EQ(max_cycle_length(56, 3), 113u);
}

TEST(CollatzFixture, max_cycle_length_11) {
    ASSERT_EQ(max_cycle_length(23, 35), 112u);
}

TEST(CollatzFixture, max_cycle_length_12) {
    ASSERT_EQ(max_cycle_length(65, 100), 119u);
}

TEST(CollatzFixture, max_cycle_length_13) {
    ASSERT_EQ(max_cycle_length(34, 21), 112u);
}

TEST(CollatzFixture, max_cycle_length_14) {
    ASSERT_EQ(max_cycle_length(9, 12), 20u);
}

TEST(CollatzFixture, max_cycle_length_15) {
    ASSERT_EQ(max_cycle_length(60, 23), 113u);
}

TEST(CollatzFixture, max_cycle_length_16) {
    ASSERT_EQ(max_cycle_length(3432, 2344), 217u);
}

TEST(CollatzFixture, max_cycle_length_17) {
    ASSERT_EQ(max_cycle_length(12943, 39429), 324u);
}

TEST(CollatzFixture, max_cycle_length_18) {
    ASSERT_EQ(max_cycle_length(32349, 34933), 311u);
}

TEST(CollatzFixture, max_cycle_length_19) {
    ASSERT_EQ(max_cycle_length(324324, 98434), 443u);
}

