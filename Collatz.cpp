// -----------
// Collatz.cpp
// -----------

// --------
// includes
// --------

#include <cassert> // assert
#include <iostream>

#include "Collatz.hpp"

using namespace std;

// ----------------
// max_cycle_length
// ----------------
unsigned cache [1000000]; //cache to be used for lazy caching

unsigned max_cycle_length (long i, long j)
{
    assert(i > 0);
    assert(j > 0);
    if (i > j)
    {
        unsigned k = i;
        i = j;
        j = k; //making sure i is the smaller value and j is the larger value
    }
    if ((j/2)+1 > i)
    {
        i = (j/2) + 1; //algorithm in class in order to reduce range for mcl
    }
    unsigned curr_max_cyc = 1;
    unsigned len = 0;

    while (i <= j) //cycles through all of the numbers in range
    {
        len = calc_cycle_length(i); //calls helper
        if (len > curr_max_cyc)
        {
            curr_max_cyc = len; //if a greater mcl is found, replace value of curr_max_cyc
        }
        i++;
    }

    assert(curr_max_cyc > 0);
    return curr_max_cyc;

}

unsigned calc_cycle_length(long n)
{
    assert (n > 0);
    long orig = n;
    unsigned c = 1;
    while (n > 1)
    {
        if (n < 1000000 && cache[n] != 0) //checks if number is within the cache
        {
            c += cache[n] - 1; //if number is found, add to current cycles and return
            assert (c > 0);
            return c;
        }
        else if ((n % 2) == 0) //if even, divide by 2
        {
            n /= 2;
            c++;
        }
        else //algorithm in class for odd numbers; skips one cycle for speed
        {
            n = n + (n >> 1) + 1;
            c+=2;
        }
    }
    assert(c > 0);
    if (orig < 1000000)
    {
        cache[orig] = c; //add found mcl to cache for future reference
    }
    return c;
}
