# CS371g: Generic Programming Collatz Repo

* Name: Adithya Arunganesh

* EID: aa86362

* GitLab ID: adithyaarun

* HackerRank ID: adithya_arungan1

* Git SHA: f1c7483920d42837948daf0ef0122f2462b85b9c

* GitLab Pipelines: https://gitlab.com/adithyaarun/cs371g-collatz/-/pipelines

* Estimated completion time: 8

* Actual completion time: 14

* Comments: calc_cycle_length draws inspiration from code covered in class with few alterations
